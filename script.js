const button = document.getElementById('button');
const buttonLoad = document.getElementById('buttonload');
const audioElement = document.getElementById('audio');

// Disable/enable button
function toggleButton() {
	button.disabled = !button.disabled;
}

// Show spinner
function showSpinner() {
	// Disable Button
	toggleButton();
	button.classList.add('hidden');
	buttonLoad.classList.remove('hidden');
}

// Remove spinner
function removeSpinner() {
	button.classList.remove('hidden');
	buttonLoad.classList.add('hidden');
}

// Passing joke to VoiceRSS API
function tellMe(joke) {
	const jokeString = joke.trim().replace(/ /g, '%20');
	VoiceRSS.speech({
		key: 'e4cfe7cce9d9448c9c0ede044c8b0125',
		src: jokeString,
		hl: 'en-us',
		r: 0,
		c: 'mp3',
		f: '44khz_16bit_stereo',
		ssml: false,
	});
}

// Get jokes from jokeAPI
async function getJoke() {
	// Show spinner
	showSpinner();

	let joke = '';
	const apiUrl =
		'https://sv443.net/jokeapi/v2/joke/Programming?blacklistFlags=nsfw,religious,political,racist,sexist';
	try {
		const response = await fetch(apiUrl);
		const data = await response.json();
		if (data.setup) {
			joke = `${data.setup} ... ${data.delivery}`;
		} else {
			joke = data.joke;
		}

		// Text-to-speech
		tellMe(joke);
		// Remove spinner
		removeSpinner();
	} catch (err) {
		console.log('Whoops', err);
	}
}

// Event listeners
button.addEventListener('click', getJoke);
audioElement.addEventListener('ended', toggleButton);
